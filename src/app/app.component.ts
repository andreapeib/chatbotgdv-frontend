import {Component, ElementRef, ViewChild} from '@angular/core';
import {ChatService} from "./chat-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private chatService: ChatService) {
  }

  @ViewChild('chatListContainer') list?: ElementRef<HTMLDivElement>;
  chatInputMessage: string = "";
  human = {
    id: 1,
    profileImageUrl: 'https://www.geelongbmw.com.au/images/team/place-holder.jpg'
  }

  ngOnInit() {
    if (localStorage.getItem('fakeUserId') === null) {
      localStorage.setItem('fakeUserId', this.generateFakeId())
    } else {
      this.chatService.findAll().subscribe(data => {
        if (data !== null) {
          this.setConversation(data['conversation'])
        }
      });
    }
  }

  bot = {
    id: 2,
    profileImageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSh7RF-fbfrapXnxKUNJHjuT7jh-pgpr3QAktl3qj1qvAuWCpIrmcjELJ9EATfUMP30Itw&usqp=CAU'
  }

  chatMessages: {
    user: any,
    message: string
  }[] = [
    {
      user: this.bot,
      message: "Welcome to the GDV DL. How can I assist you?"
    },
  ];

  title = 'frontend';

  ngAfterViewChecked() {
    this.scrollToBottom()
  }

  send() {
    this.chatMessages.push({
      message: this.chatInputMessage,
      user: this.human
    });
    this.chatService.send(this.chatInputMessage).subscribe(data => {
      this.receive(data);
    });
    this.chatInputMessage = ""
    this.scrollToBottom()
  }

  receive(message: string) {
    this.chatMessages.push({
      message: message,
      user: this.bot
    });
    this.scrollToBottom()
  }

  setConversation(conversation: any) {
    for (let i = 0; i < conversation.length; i++) {
      if (i % 2 === 0) {
        this.chatMessages.push({
          message: conversation[i],
          user: this.human
        });
      } else {
        this.chatMessages.push({
          message: conversation[i],
          user: this.bot
        });
      }
    }
  }

  scrollToBottom() {
    const maxScroll = this.list?.nativeElement.scrollHeight;
    this.list?.nativeElement.scrollTo({ top: maxScroll, behavior: 'smooth' });
  }

  generateFakeId(): string {
    const current = new Date();
    const timestamp = current.getTime();
    return timestamp.toString()
  }

  clearConversation() {
    localStorage.removeItem('fakeUserId')
    localStorage.setItem('fakeUserId', this.generateFakeId())
    this.chatService.deleteConversation()
    this.chatMessages = [
      {
        user: this.bot,
        message: "hi, I'm an AI. You can start any conversation..."
      },
    ];
  }
}
